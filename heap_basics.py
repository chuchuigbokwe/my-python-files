import math
import heapq
from cStringIO import StringIO


def show_tree(tree, total_width=36, fill=' '):
    """Pretty-print a tree."""
    output = StringIO()
    last_row = -1
    for i, n in enumerate(tree):
        if i:
            row = int(math.floor(math.log(i+1, 2)))
        else:
            row = 0
        if row != last_row:
            output.write('\n')
        columns = 2**row
        col_width = int(math.floor((total_width * 1.0) / columns))
        output.write(str(n).center(col_width, fill))
        last_row = row
    print output.getvalue()
    print '-' * total_width
    print
    return


def show_heap():
    data = []
    heap = []
    how_many = int(input('Enter how many numbers do you want in your data set?:'))
    print 'You are going to enter',how_many,'numbers into your data set'
   
    for i in range(how_many):
	x = int(input('Enter a number: '))
	data.append(x)
    print 'This is the data set',data
    print

    for n in data:
        print 'add %3d:' % n
        heapq.heappush(heap, n)
        show_tree(heap)

show_heap()
