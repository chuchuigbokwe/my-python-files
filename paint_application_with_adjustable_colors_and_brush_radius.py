import cv2
import numpy as np

def nothing(x):
    pass

# mouse callback function
def draw_circle(event, x,y,flags,param):
    '''
     a simple application which draws a circle on an image 
     wherever we double-click on it.
    '''
    if event == cv2.EVENT_LBUTTONDBLCLK:
        cv2.circle(img,(x,y),radius,(255,255,255),-1)
        
#Create a black image, a window and bind the function to the window
img = np.zeros([512,512,3])#, np.uint8)
cv2.namedWindow('image')
cv2.setMouseCallback('image',draw_circle) #a mouse callback

cv2.createTrackbar('width','image',0,100,nothing)
cv2.createTrackbar('R','image', 0,255,nothing)
cv2.createTrackbar('G','image', 0,255,nothing)
cv2.createTrackbar('B','image', 0,255,nothing)
switch = '0: OFF \n1 : ON'
cv2.createTrackbar(switch,'image', 0,1,nothing)


while(1):
    cv2.imshow('image', img)
    
    r = cv2.getTrackbarPos('R','image')
    g = cv2.getTrackbarPos('G','image')
    b = cv2.getTrackbarPos('B','image')
    s = cv2.getTrackbarPos(switch,'image')
    radius= cv2.getTrackbarPos('width','image')
    if cv2.waitKey(20) & 0xFF == 27:
        break
# Once I try including coloours i can't draw circles anymore, I'll figure
#       this out later
#    if s == 0:
#       img[:] = 0
#       radius= cv2.getTrackbarPos('width','image')
#    else:
#        img[:] = [b,g,r]
#        radius= cv2.getTrackbarPos('width','image')

    
    
cv2.destroyAllWindows()