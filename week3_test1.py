#!/usr/bin/python
import cv2, numpy as np
from numpy.linalg import inv


## 1. Extract SURF keypoints and descriptors from an image. [4] ----------
def extract_features(image, surfThreshold=1000):

	## TODO: Convert image to grayscale (for SURF detector).
	image_grayscale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	## TODO: Detect SURF features and compute descriptors.
	surf = cv2.SURF()
	## TODO: (Overwrite the following 2 lines with your answer.)
	(kp, des) = surf.detectAndCompute(image_grayscale,None)

	return (kp, des)


## 2. Find corresponding features between the images. [2] ----------------
def find_correspondences(keypoints1, descriptors1, keypoints2, descriptors2):

	## Find corresponding features.
	match = match_flann(descriptors1, descriptors2)

	## TODO: Look up corresponding keypoints.
	points1 = match[:,0]
	points2 = match[:,1]
	## TODO: (Overwrite the following 2 lines with your answer.)
	points11 = np.array([k.pt for k in keypoints1], np.float32)
	points21 = np.array([k.pt for k in keypoints2], np.float32)

	cord1 = np.full( (len(points1),2), None)
	cord2 = np.full( (len(points1),2), None)

	i = 0
	while i<len(points1):
		cord1[i,0] = points11[ points1[i] ][0]
		cord1[i,1] = points11[ points1[i] ][1]
		cord2[i,0] = points21[ points2[i] ][0]
		cord2[i,1] = points21[ points2[i] ][1]
		i = i+1

	return (cord1, cord2)


## 3. Calculate the size and offset of the stitched panorama. [5] --------
def calculate_size(size_image1, size_image2, homography):

	## TODO: Calculate the size and offset of the stitched panorama.
	## TODO: (Overwrite the following 2 lines with your answer.)
	A = np.array([[0],[0],[1]])
	B = np.full((3,1), None)

	H_inv = inv(homography)
	B = np.dot(H_inv,A)

	offset = (-int(B[0,0]), -int(B[1,0]))
	size   = (size_image1[1] + offset[0], size_image1[0] + offset[1])

	## Update the homography to shift by the offset
	homography_updated = np.full((3,3),None)
	homography_updated[:,:] = homography[:,:]
	homography_updated[0,2] += offset[0]
	homography_updated[1,2] += offset[1]
	print "homography updated: \n",homography_updated
	print "homography123: \n",homography


	return (size, offset, homography_updated)


## 4. Combine images into a panorama. [4] --------------------------------
def merge_images(image1, image2, H, H_upd, size, offset, keypoints):

	## TODO: Combine the two images into one.
	## TODO: (Overwrite the following 5 lines with your answer.)
	(h1, w1) = image1.shape[:2]
	(h2, w2) = image2.shape[:2]
	panorama = np.zeros((size[1], size[0], 3), np.uint8)

	print "H: \n",H
	print "H_upd: \n",H_upd
	print "Size: ",size

	image1_warped = cv2.warpPerspective(image1, H, (h1,size[0]/2))
	image2_warped = cv2.warpPerspective(image2, inv(H_upd), (size[1],size[0]/2))

	panorama[0:size[0]/2, 0:h1] = image1_warped
	#panorama[:, w1:w1+w2] = image2_warped

	## TODO: Draw the common feature keypoints.

	return panorama





def warpTwoImages(img1, img2, H):
    '''warp img2 to img1 with homograph H'''
    h1,w1 = img1.shape[:2]
    h2,w2 = img2.shape[:2]
    pts1 = np.float32([[0,0],[0,h1],[w1,h1],[w1,0]]).reshape(-1,1,2)
    pts2 = np.float32([[0,0],[0,h2],[w2,h2],[w2,0]]).reshape(-1,1,2)
    pts2_ = cv2.perspectiveTransform(pts2, H)
    pts = np.concatenate((pts1, pts2_), axis=0)
    [xmin, ymin] = np.int32(pts.min(axis=0).ravel() - 0.5)
    [xmax, ymax] = np.int32(pts.max(axis=0).ravel() + 0.5)
    t = [-xmin,-ymin]
    Ht = np.array([[1,0,t[0]],[0,1,t[1]],[0,0,1]]) # translate

    result = cv2.warpPerspective(img2, Ht.dot(H), (xmax-xmin, ymax-ymin))
    result[t[1]:h1+t[1],t[0]:w1+t[0]] = img1
    return result






def match_flann(desc1, desc2, r_threshold = 0.06):
	'Finds strong corresponding features in the two given vectors.'
	## Adapted from <http://stackoverflow.com/a/8311498/72470>.

	## Build a kd-tree from the second feature vector.
	FLANN_INDEX_KDTREE = 1  # bug: flann enums are missing
	flann = cv2.flann_Index(desc2, {'algorithm': FLANN_INDEX_KDTREE, 'trees': 4})

	## For each feature in desc1, find the two closest ones in desc2.
	(idx2, dist) = flann.knnSearch(desc1, 2, params={}) # bug: need empty {}

	## Create a mask that indicates if the first-found item is sufficiently
	## closer than the second-found, to check if the match is robust.
	mask = dist[:,0] / dist[:,1] < r_threshold

	## Only return robust feature pairs.
	idx1  = np.arange(len(desc1))
	pairs = np.int32(zip(idx1, idx2[:,0]))
	return pairs[mask]


def draw_correspondences(image1, image2, points1, points2):
	'Connects corresponding features in the two images using yellow lines.'

	## Put images side-by-side into 'image'.
	(h1, w1) = image1.shape[:2]
	(h2, w2) = image2.shape[:2]
	image = np.zeros((max(h1, h2), w1 + w2, 3), np.uint8)
	image[:h1, :w1] = image1
	image[:h2, w1:w1+w2] = image2

	## Draw yellow lines connecting corresponding features.
	for (x1, y1), (x2, y2) in zip(np.int32(points1), np.int32(points2)):
		cv2.line(image, (x1, y1), (x2+w1, y2), (0, 255, 255), lineType=cv2.CV_AA)

	return image


if __name__ == "__main__":

	## Load images.
	image1 = cv2.imread("/home/chu-chu/Documents/Python27/CS4243_Group1_ImageStitching/test_images/panorama_image1.jpg")
	image2 = cv2.imread("/home/chu-chu/Documents/Python27/CS4243_Group1_ImageStitching/test_images/panorama_image2.jpg")

	## Detect features and compute descriptors.
	(keypoints1, descriptors1) = extract_features(image1)
	(keypoints2, descriptors2) = extract_features(image2)
	print len(keypoints1), "features detected in image1"
	print len(keypoints2), "features detected in image2"

	## Find corresponding features.
	(points1, points2) = find_correspondences(keypoints1, descriptors1, keypoints2, descriptors2)
	print len(points1), "features matched"

	## Visualise corresponding features.
	correspondences = draw_correspondences(image1, image2, points1, points2)
	cv2.imwrite("correspondences.jpg", correspondences)
	#cv2.imshow('correspondences', correspondences)

	## Find homography between the views.
	(homography, _) = cv2.findHomography(points2, points1)
	print "Homography: \n", homography



	panorama = warpTwoImages(image1,image2,homography)






	## Calculate size and offset of merged panorama.
	#(size, offset, homography_updated) = calculate_size(image1.shape, image2.shape, homography)
	#print "output size: %ix%i" % size

	## Finally combine images into a panorama.
	#panorama = merge_images(image1, image2, homography, homography_updated, size, offset, (points1, points2))
	cv2.imwrite("panorama.jpg", panorama)
	cv2.imshow('panorama', panorama)
	
	## Show images and wait for escape key.
	while cv2.waitKey(100) != 27:
		pass
