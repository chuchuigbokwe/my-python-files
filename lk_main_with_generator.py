# -*- coding: utf-8 -*-
"""
Created on Sun Feb  1 17:11:14 2015

@author: chu-chu
"""

import lktrack_class
imnames = ['rubic/rubic.0.bmp', 'rubic/rubic.1.bmp', 'rubic/rubic.2.bmp','rubic/rubic.3.bmp']
# track using the LKTracker generator
lkt = lktrack_class.LKTracker(imnames)
for im,ft in lkt.track():
    print 'tracking %d features' % len(ft)
# plot the tracks
plt.figure()
cv2.imshow('x',im)
for p in ft:
    plt.plot(p[0],p[1],'bo')
for t in lkt.tracks:
    plt.plot([p[0] for p in t],[p[1] for p in t])
plt.axis('off')
plt.show()