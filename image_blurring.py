#!/usr/bin/env python

import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('opencv_logo.png')

blur = cv2.blur(img,(5,5))
gaussianblur = cv2.GaussianBlur(img,(5,5),0)
median = cv2.medianBlur(img,5) #50% noise to our original image and use a median filter
bilateralfilter = cv2.bilateralFilter(img,9,75,75)


plt.subplot(321),plt.imshow(img),plt.title('Original')
plt.xticks([]), plt.yticks([])

plt.subplot(322),plt.imshow(blur),plt.title('Blurred')
plt.xticks([]), plt.yticks([])

plt.subplot(323),plt.imshow(gaussianblur),plt.title('Gaussian Blur')
plt.xticks([]), plt.yticks([])

plt.subplot(324),plt.imshow(median),plt.title('Median')
plt.xticks([]), plt.yticks([])

plt.subplot(325),plt.imshow(bilateralfilter),plt.title('Bilateral Filter')
plt.xticks([]), plt.yticks([])

plt.show()