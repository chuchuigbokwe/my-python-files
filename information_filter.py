#!/usr/bin/env python
"""
Created on Fri Oct 10 11:28:15 2014

@author: chu-chu
"""

import numpy as np 
import matplotlib.pyplot as plt
x = 0
y = 0
theta = 0
vel = []
omega = []
timechange = []
xpr = []
ypr = []
thetapr = []
velerror = []
omegaerror = []
gammaerror = []
x_coordinates = [0]
y_coordinates = [0]
heading_angle = [0]
redrobin = np.loadtxt('ds1_Odometry.dat')
batwing = np.loadtxt('ds1_Groundtruth.dat')
batman = np.loadtxt('ds1_Measurement.dat')
landmarks = np.loadtxt('ds1_Landmark_Groundtruth.dat')


def simulatedController(v,w,delta_t):   #dead reckoning controller
    global x,y,theta,x_coordinates,y_coordinates
    theta = theta + w * delta_t 
    x = x + v * delta_t * np.cos(theta) 
    y = y + v * delta_t * np.sin(theta) 
    heading_angle.append(theta)
    x_coordinates.append(x)
    y_coordinates.append(y)
    #return x_coordinates,y_coordinates, heading_angle


def SimConVerification():
    ''' I verified my controller with the values given in question 2'''
    nightwing = np.array([[0.5,0,1],[0,-1/np.pi*2,1],[0.5,0,1],[0,1/np.pi*2,1]\
    ,[0.5,0,1]])
    dim = np.shape(nightwing)
    xpos = [0]
    ypos = [0]
    thetapos = [0]
    x = 0
    y = 0
    theta = 0
    row = 0
    column = 0
    for i in range(dim[0]): #parse through the matrix form left to right and
    #descend a row
        if row< dim[0] and column < dim[1]:
            v = nightwing[row][column]
            column +=1
            w = nightwing[row][column]
            column +=1
            delta_t = nightwing[row][column]
            #simulatedController(v,w,delta_t)
            
            theta = theta + w * delta_t 
            x = x + v * delta_t * np.cos(theta) 
            y = y + v * delta_t * np.sin(theta) 
            
            thetapos.append(theta)
            xpos.append(x)
            ypos.append(y)
            row +=1
            column = 0
        else:
            break
    print 'These are the x co-ordinates',xpos
    print 
    print 'These are the y co-ordinates', ypos
    print
    print 'These are the heading angles in radians', thetapos
    
    plt.title('Verification of Simulated Controller with Values Given in \
    Question 2')
    plt.ylabel('Y-Axis')
    plt.xlabel('X-Axis')
    plt.plot(xpos,ypos)
    plt.show()
    



def SimConWithRobotDataset():
    
    dim = np.shape(redrobin)
    row = 0
    column = 0
    
    t1 = redrobin[0][0]
    for i in range(dim[0]):
        if row< dim[0] and column < dim[1]:#parse through the matrix form left to right and
    #descend a row
            if row == 0:
               delta_t = 0
               timechange.append(delta_t)
            else:
                delta_t = redrobin[row][column] - redrobin[row-1][column]
                timechange.append(delta_t)
            column +=1
            v = redrobin[row][column]
            column +=1
            w = redrobin[row][column]
            simulatedController(v,w,delta_t)
            vel.append(v)
            omega.append(w)
            row +=1
            column = 0
        else:
            break
    return timechange


def Groundtruth():
    batwing = np.loadtxt('ds1_Groundtruth.dat')
    dim = np.shape(batwing)
    timestampground = [0]
    xaxis = [0]
    yaxis = [0]
    row = 0
    column = 0
    for i in range(dim[0]):
        if row< dim[0] and column < dim[1]:
            t = batwing[row][column]
            column +=1
            xvalue = batwing[row][column]
            column +=1
            yvalue = batwing[row][column]
            column +=1
            orientation = batwing[row][column]
            
            xaxis.append(xvalue)
            yaxis.append(yvalue)
            timestampground.append(t)
            row +=1
            column = 0
        else:
            break
  
    plt.title('ds1_Groundtruth.dat Plotted with Odometry from Simulated Controller')
    plt.ylabel('Y-Axis')
    plt.xlabel('X-Axis')
    plt.plot(xaxis,yaxis, '-b', label='Groundtruth') 
    plt.plot(x_coordinates,y_coordinates, '-r', label='Odometry')
    plt.legend(loc='upper right')
    plt.show()



def convtoarrays():
    xarray = np.asarray(x_coordinates)
    yarray = np.asarray(y_coordinates)
    thetaarray = np.asarray(heading_angle)
    a = np.vstack((xarray,yarray,thetaarray.T))
    b = a.T
    print np.shape(a)     
    return a
    
    


def MotionModel():
    A = np.identity(3)
    newx = []
    newy = []
    newheading = []
    for i in range(len(timechange)):
        x = np.array([[x_coordinates[i]],[y_coordinates[i]],[heading_angle[i]]])
        B = np.array([[0,.0],[1,0],[0,timechange[i]]])
        U = np.array([[vel[i]],[omega[i]]])
        F = np.dot(A,x) + np.dot(B,U)   #implementing motion model
        newx.append(F[0])
        newy.append(F[1])
        newheading.append(F[2])
        i += 1
        
    plt.plot(newx,newy, 'g', label = 'motion model')
    plt.legend(loc = 'upper right')
    plt.show()
    
def MeasurementModel():
    for i in range(len(batman)):
        xtrue = []
        ytrue = []
        x = x_coordinates[i] + batman[i][2]* np.sin(heading_angle[i]-batman[i][3])
        y = y_coordinates[i] + batman[i][2]* np.cos(heading_angle[i]-batman[i][3])
        xtrue.append(x)
        ytrue.append(y)
        i+=1
    return xtrue, ytrue
        
        
#def PredictionStep()
#    A = np.identity(3)
#    xp = []
#    yp = []
#    tp = []
#    ohm = 0.1 * np.matlib.identity(3)
#    for i in range(100):
#        M = np.array([[x_coordintes[i],[batwing.T[1][i]]],[[y_coordinates[i]],[batwing.T[2][i]]],[heading_anle[i],batwing.T[3][i]]])
#        R = np.cov(M)          
#        ohmprime = np.dot(A,ohm,A.T) + R
#        psiprime = ohm * np.dot(A,ohmprime,psi) + [y_coordinates[i],np.dot(B,U)
#    
#    
#
#PredictionStep()
def main():
    SimConVerification()
    SimConWithRobotDataset()
    MotionModel()
    Groundtruth()
    convtoarrays()
    MeasurementModel()
    
if __name__ == '__main__':
    main()
