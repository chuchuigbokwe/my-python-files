# -*- coding: utf-8 -*-
"""
Created on Sat Jan 17 14:05:21 2015

@author: chu-chu
"""

import numpy as np
import cv2
from matplotlib import pyplot as plt

img = cv2.imread('messi4.jpg',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.xticks([]), plt.yticks([]) #to hide tick values on X and Y
plt.show()