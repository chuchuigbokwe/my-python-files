#!/usr/bin/env python
"""
Created on Thu Jun 25 13:30:02 2015

@author: chu-chu
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt

im = cv2.imread('messi5.jpg')
imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
ret,thresh = cv2.threshold(imgray,127,255,0)
image, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
img = cv2.drawContour(im, contours, -1, (0,255,0), 3)
cv2.imshow('Image1',img)