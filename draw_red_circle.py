#!/usr/bin/env python
import rospy
import cv2
import numpy as np
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

bridge = CvBridge()

def drawRedCircle(cv_image):
    cv2.namedWindow("Test Window", 1)
    grayIm = cv2.cvtColor(cv_image,cv2.COLOR_BGR2GRAY)     
    
    circles = None    
    circles = cv2.HoughCircles(grayIm,cv2.cv.CV_HOUGH_GRADIENT, 1.2, 100)
    
    if circles is not None:
        circles = np.round(circles[0, :]).astype("int")
        for (x, y, r) in circles:
                cv2.circle(cv_image, (x, y), r, (0, 255, 0), 4)    

    cv2.imshow("Test Window", cv_image)
    cv2.waitKey(3)

def callback(ros_image):
    cv_image = bridge.imgmsg_to_cv2(ros_image, desired_encoding="bgr8")
    drawRedCircle(cv_image)
    

def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber("/camera_node/image_raw",Image,callback);
    #rospy.sleep(10000)
    rospy.spin()

if __name__ == '__main__':
    listener()