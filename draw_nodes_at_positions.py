# -*- coding: utf-8 -*-
"""
Created on Mon Dec  8 19:28:14 2014

@author: chu-chu
"""

import networkx as nx
import itertools
import matplotlib.pyplot as plt

pos = {0: (40, 20), 1: (20, 30), 2: (40, 30), 3: (30, 10)} 
X=nx.Graph()
X.add_nodes_from(pos.keys())
for n, p in pos.iteritems():
    X.node[n]['pos'] = p

nx.draw(X, pos)
plt.xlim([0,100])
plt.ylim([0,100])
plt.show()