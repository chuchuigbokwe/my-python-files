#!/usr/bin/env/python
"""
Created on Tue Dec  9 18:50:27 2014

@author: chu-chu
"""

import numpy as np
import matplotlib.pyplot as plt
import time
import itertools
import math
from shapely.geometry import Point
from shapely.geometry import MultiPoint
import random

#creates a start and finish node
def StartandGoalNodes():
    '''
    It asks for the start and goal nodes.
    '''
    start_x = float(input("Enter the x co-ordinates for the start node:"))
    start_y = float(input("Enter the y co-ordinates for the start node:"))
    goal_x = float(input("Enter the x co-ordinates for the goal node:"))
    goal_y = float(input("Enter the y co-ordinates for the goal node:"))
    target = np.array([[start_x,start_y],[goal_x,goal_y]])
    return target

#creates inpenetrable obstacles
#def createObstacles(size, targets, quantity):
#	radii = np.array([5,10]) #the range of possible radii for the obstacles
#	obstacles = np.zeros([quantity, 3]) #Nx3 matrix, columns are X,Y, and radius
#
#	#creates a random set of obstacles of random size
#	for i in range(quantity):
#		check = False #used for verifying if a node intercepts the targets
#		while check == False:
#			#try to create a node that does not overlap the targets
#			nodeX = random.uniform(size[0],size[1])
#			nodeY = random.uniform(size[2],size[3])
#			nodeRadius = random.uniform(radii[0],radii[1])
#			check = checkIntersection(targets, nodeX, nodeY, nodeRadius)
#		obstacles[i,0] = nodeX
#		obstacles[i,1] = nodeY
#		obstacles[i,2] = nodeRadius
#	return obstacles

def CreateObstacles():
    NumberofObstacles = int(input('How many obstacles are there? '))
    obstacles = np.empty([NumberofObstacles,3])
    robot_radius = float(input("Enter the radius of the robot:"))
    for i in range(NumberofObstacles):
        obstacle_x = float(input("Enter the x co-ordinates for the obstacle:"))
        obstacles[i][0] = obstacle_x
        obstacle_y = float(input("Enter the y co-ordinates for the obstacle:"))
        obstacles[i][1] = obstacle_y
        obstacle_radius = float(input("Enter the radius of the obstacle:"))
        obstacle_radius += robot_radius
#        I added the robots radius here
        obstacles[i][2] = obstacle_radius
        
    print obstacles
    return obstacles

#checks to see if a coordinate intersects an obstacle
def checkIntersection(targets, nodeX, nodeY, nodeRadius):
	#checks to see if the prospective nodes intersect with the start or finish targets
	for i in range(len(targets)):
		distance = np.sqrt(np.power(targets[i,0]-nodeX,2)+np.power(targets[i,1]-nodeY,2))
		if distance < nodeRadius:
			# print 'got an intersection at', nodeX, nodeY, nodeRadius
			return False
	return True

#creates a node that the robot could move to
def createNodes(size, obstacles, quantity):
	nodes = np.zeros([quantity, 2]) #columns are X and Y
	for i in range(quantity):
		check = False
		while check == False:
			#try to create a node that does not overlap the obstacles
			nodeX = random.uniform(size[0],size[1])
			nodeY = random.uniform(size[2],size[3])
			#checks for interception on each individual obstacle
			for j in range(len(obstacles)):
				targets = np.matrix([nodeX, nodeY])
				test = checkIntersection(targets, obstacles[j,0], obstacles[j,1], obstacles[j,2])
				if test == False:
					break
			if test:
				check = True
				nodes[i,0] = nodeX
				nodes[i,1] = nodeY
        print len(nodes)
	return nodes
        
     

def CreateNodes():
    '''
    It asks for the number of nodes "N" and asks you to input the x and y 
    co-ordinate of each node.
    '''
    numberofnodes = int(input('How many nodes are there? '))
    nodes = np.empty([numberofnodes,2])
    for i in range(numberofnodes):
        node_x = float(input("Enter the x co-ordinates for the node:"))
        nodes[i][0] = node_x
        node_y = float(input("Enter the y co-ordinates for the node:"))
        nodes[i][1] = node_y
    
    print nodes
    return nodes

def freenodes(nodes,obstacles):
    freenodes = nodes
    for i in range(len(nodes)):
        for j in range(len(obstacles)):
            distance = np.sqrt(np.power(nodes[i][0]-obstacles[j][0],2)+np.power(nodes[i][1]-obstacles[j][1],2))
            if distance < obstacles[j][2]:          
                freenodes =  np.delete(freenodes,i,0)  
            else:
                continue
    print freenodes
    print len(freenodes)
    return freenodes

def createEdges(nodes, obstacles, targets):
	
	nodes = np.vstack([nodes,targets])
	edgeDict = {}

	for i in nodes:
		edgeDict[hash(str(i))] = None

	for i in itertools.combinations(nodes,2):
		m = (i[1][1]-i[0][1])/(i[1][0]-i[0][0])
		thetaInv = math.atan(-1/m)
		intercept = i[1][1] - m*i[1][0]
		arrayX = np.array([i[0][0],i[1][0]])
		arrayY = np.array([i[0][1],i[1][1]])

		test = True
		for j in range(len(obstacles)):
			poly = np.zeros([4,2])
			dX = (obstacles[j,2]+radius)*np.cos(thetaInv)
			dY = (obstacles[j,2]+radius)*np.sin(thetaInv)

			poly[0,0] = i[0][0] + dX
			poly[0,1] = i[0][1] + dY
			poly[1,0] = i[0][0] - dX
			poly[1,1] = i[0][1] - dY
			poly[2,0] = i[1][0] + dX
			poly[2,1] = i[1][1] + dY
			poly[3,0] = i[1][0] - dX
			poly[3,1] = i[1][1] - dY	

			poly = MultiPoint(poly).convex_hull
			point = Point(obstacles[j,0],obstacles[j,1])

			if poly.contains(point):
				test = False
				break

		
		if test:
			try:
				edgeDict[hash(str(i[0]))] = np.vstack([edgeDict[hash(str(i[0]))],i[1]])
			except:
				edgeDict[hash(str(i[0]))] = i[1]
			try:
				edgeDict[hash(str(i[1]))] = np.vstack([edgeDict[hash(str(i[1]))],i[0]])
			except:
				edgeDict[hash(str(i[1]))] = i[0]

	return edgeDict

#finds euclidian distance
def evalHeuristic(current,goal):
	try:
		return np.sqrt(np.power(goal[1]-current[1],2)+np.power(goal[0]-current[0],2))
	except:
		print 'error in evalHeuristic.','\n',current,'\n',goal
def evalTrueCost(current, target, priorCost):
	return np.sqrt(np.power(target[1]-current[1],2)+np.power(target[0]-current[0],2)) + priorCost

#the big momma
def aStar(nodes, targets, edges):
	untouchedNodes = np.vstack([nodes,targets]) 
	totalCostList = []  
	trueCostList = []  
	expandedList = [] 
	parentList = [] 
	touchedNodes = [] 

	start = targets[0]
	goal = targets[1]
	current = start

	touchedNodes.append(current)
	parentList.append(current)
	#expand your first node
	for newEdge in edges[hash(str(current))]:
		heuristic = evalHeuristic(newEdge, goal) 
		trueCost = evalTrueCost(current,newEdge,0) #finds distance traversed to find this node

		#update lists
		totalCost = heuristic + trueCost
		trueCostList.append(trueCost)
		totalCostList.append(totalCost)
		expandedList.append(newEdge)
		parentList.append(current)
		touchedNodes.append(newEdge)

	lowestCostIndex = totalCostList.index(min(totalCostList))
	current = expandedList[lowestCostIndex]
	heuristic = evalHeuristic(current, goal)
	parentCost = trueCostList[lowestCostIndex]
	count = 0


	while heuristic != 0:
		expandedList.pop(lowestCostIndex)
		trueCostList.pop(lowestCostIndex)
		totalCostList.pop(lowestCostIndex)

		#expand your new node
		for newEdge in edges[hash(str(current))]:

			skip = False
			for touched in touchedNodes:	
				try:
					if newEdge[0] == touched[0] and newEdge[1] == touched[1]:
						skip = True
						break
				except:
					print "Game over."
					skip = True
					break
			if skip:
				continue
			heuristic = evalHeuristic(newEdge, goal)
			trueCost = evalTrueCost(current,newEdge,parentCost)
			totalCost = heuristic + trueCost
			trueCostList.append(trueCost)
			totalCostList.append(totalCost)
			expandedList.append(newEdge)
			parentList.append(current)
			touchedNodes.append(newEdge)
		try:
			lowestCostIndex = totalCostList.index(min(totalCostList))
		except:
			print 'Hitherto shalt thou come, but no further.'
			return False
			break
		current = expandedList[lowestCostIndex]

		heuristic = evalHeuristic(current, goal)
		parentCost = trueCostList[lowestCostIndex]
		count += 1
		if count >50:
			break
	path = []
	index = lowestCostIndex
	count = 0
	while round(current[0],6) != round(start[0],6) and round(current[1],6) != round(start[1],6):
		for i in range(len(touchedNodes)):
			if current[0] == touchedNodes[i][0] and current[1] == touchedNodes[i][1]:
				index = i
				break
		parent = parentList[index]
		path.append(parent)
		current = parent
		count += 1
		if count > 20:
			break
	print path
	current = goal
	for i in path:
		plt.plot([current[0],i[0]],[current[1],i[1]], linewidth = 1, color = 'b')
		current = i

radius = 0
size = np.array([0,100,0,100])
targets = StartandGoalNodes()
obstacles = CreateObstacles() 
nodes = createNodes(size, obstacles,10)
freenodes(nodes,obstacles)

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
edges = createEdges(nodes, obstacles, targets)
a = aStar(nodes, targets, edges)
for i in range(len(nodes)):
	circ = plt.Circle((nodes[i,0],nodes[i,1]), radius = .5, color='black')
	ax.add_patch(circ)
for i in range(len(obstacles)):
	circ = plt.Circle((obstacles[i,0],obstacles[i,1]), radius = obstacles[i,2],alpha=0.5, color='blue')
	ax.add_patch(circ)

plt.plot(targets[0,0], targets[0,1],'r^', markersize = 10)
plt.plot(targets[1,0], targets[1,1],'rs', markersize = 10)

plt.axis(size, aspect=1)	

end = time.time()
c = -start + end

print 'start at:',targets[0]
print 'end at:',targets[1]
print ''

plt.show()






