import sys
sys.stdout.flush()
def collatz(n):
    '''
    Print the 3n+1 sequence from n, terminating when it reaches 1.
    '''
    a = []
    while n!= 1:
        a.append(n)
        if n % 2 == 0:
            n = n // 2
            
        else:
            n = n * 3 + 1
    a.append(n)        
    print(a) 
    print 'the longest sequence is',len(a)
collatz(1000000)
