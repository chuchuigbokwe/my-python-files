# -*- coding: utf-8 -*-
"""
Created on Thu Dec  4 01:32:45 2014

@author: chu-chu
"""

import numpy as np
def StartandGoalNodes():
    '''
    It asks for the start and goal nodes.
    '''
    start_x = float(input("Enter the x co-ordinates for the start node:"))
    start_y = float(input("Enter the y co-ordinates for the start node:"))
    goal_x = float(input("Enter the x co-ordinates for the goal node:"))
    goal_y = float(input("Enter the y co-ordinates for the goal node:"))
    target = np.array([[start_x,start_y],[goal_x,goal_y]])
    print target
    return target


def CreateNodes():
    '''
    It asks for the number of nodes "N" and asks you to input the x and y 
    co-ordinate of each node.
    '''
    numberofnodes = int(input('How many nodes are there? '))
    nodes = np.empty([numberofnodes,2])
    for i in range(numberofnodes):
        node_x = float(input("Enter the x co-ordinates for the node:"))
        nodes[i][0] = node_x
        node_y = float(input("Enter the y co-ordinates for the node:"))
        nodes[i][1] = node_y
    
    print nodes
    return nodes
    
def CreateObstacles():
    NumberofObstacles = int(input('How many obstacles are there? '))
    obstacles = np.empty([NumberofObstacles,3])
    robot_radius = float(input("Enter the radius of the robot:"))
    for i in range(NumberofObstacles):
        obstacle_x = float(input("Enter the x co-ordinates for the obstacle:"))
        obstacles[i][0] = obstacle_x
        obstacle_y = float(input("Enter the y co-ordinates for the obstacle:"))
        obstacles[i][1] = obstacle_y
        obstacle_radius = float(input("Enter the radius of the obstacle:"))
        obstacle_radius += robot_radius
#        I added the robots radius here
        obstacles[i][2] = obstacle_radius
        
    print obstacles
    return obstacles

def freenodes(nodes,obstacles):
    freenodes = nodes
    for i in range(len(obstacles)):
        x_min = obstacles[i][0] - obstacles[i][2]
        y_min = obstacles[i][1] - obstacles[i][2]
        x_max = obstacles[i][0] + obstacles[i][2]
        y_max = obstacles[i][1] + obstacles[i][2]
        for j in range(len(nodes)):
            if nodes[j][0]> x_min and nodes[j][0] < x_max and  nodes[j][1] > y_min and nodes[j][1] < y_max:
               freenodes =  np.delete(freenodes,j,0)  
            else:
                continue
#    print freenodes
    return freenodes


def MakeGraph(nodes):
    dim = len(nodes)
    Graph = np.empty([dim,dim])
    for i in range(dim):
        for j in range(dim):
            Graph[i][j] = sqrt((nodes[i][0] - nodes[j][0])**2 + (nodes[i][1] -nodes[j][1])**2)          
            
    print Graph
    
def main():
    StartandGoalNodes()
    CreateNodes()
    CreateObstacles()
    
main()
#array.tolist() --> convert numpy array to list.