# -*- coding: utf-8 -*-
"""
Created on Sun Feb  1 17:03:23 2015

@author: chu-chu
"""

import lktrack_class
imnames = ['rubic/rubic.0.bmp', 'rubic/rubic.1.bmp', 'rubic/rubic.2.bmp','rubic/rubic.3.bmp']
# create tracker object
lkt = lktrack_class.LKTracker(imnames)
# detect in first frame, track in the remaining
lkt.detect_points()
lkt.draw()
for i in range(len(imnames)-1):
    lkt.track_points()
    lkt.draw()