class Point:
    '''Point class represents and manipulates x,y coords'''
    
    def __init__(self,x=0,y=0):
        '''Create a new point at the origin'''
        self.x = x
        self.y = y
    
    def distance_from_origin(self):
        '''compute my distance from the origin'''
        return ((self.x**2)+(self.y**2))**0.5
        
#    def to_string(self):
#        return'({0},{1})'.format(self.x,self.y)
        
    def __str__(self):
        return "({0}, {1})".format(self.x, self.y)
    
    def halfway(self,target):
        '''Return the halfway point between myself and the target'''
        mx = (self.x + target.x)/2
        my = (self.y + target.y)/2
        print Point(mx,my)
        return  Point(mx,my)
        
    def reflect_x(self):
        '''reflects a point about its x-axis such that (x,y) becomes (x,-y)'''
        return (self.x,-self.y)
    
    def slope_from_origin(self):
        '''returns the slope of a line joining the originto the point'''
        origin = Point()
        slope = float((self.y - origin.y))/ (self.x - origin.x) #float division
        return slope 
        
    def get_line_to(self,secondPoint):
        '''solve the equation of a line y = mx + c for two points and return (m,c)'''
        slope = float((self.y -secondPoint.y))/ (self.x - secondPoint.x) #float division
        intercept = self.y - slope * self.x
        print 'the slope of the line is', slope,'and the intercept is',intercept,'the equation of the line is y =',slope,'x +',intercept
        return(slope,intercept)
        
   
class Rectangle:
    '''A class to manufacture rectangle objects'''
    def __init__(self,posn,w,h):
        '''Initialize rectangel at posn, with width w and height h'''
        self.corner = posn
        self.width  = w
        self.height = h
        
    def __str__(self):
        return '({0},{1},{2})'.format(self.corner,self.width,self.height)
        
    def grow(self,delta_width,delta_height):
        '''Grow (or shrink) this object by the deltas'''
        self.width += delta_width
        self.height += delta_height
        
    def move(self,dx,dy):
        '''Move this object by the deltas'''
        self.corner.x += dx
        self.corner.y += dy
        
#box = Rectangle(Point(0,0),100,200)
#bomb = Rectangle(Point(100,80),5,10)    #In my video game
#print 'box:', box		
#print 'bomb:', bomb		

def same_coordinates(p1,p2):
    return(p1.x == p2.x) and (p1.y == p2.y)