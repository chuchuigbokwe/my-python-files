# -*- coding: utf-8 -*-
"""
Created on Mon Dec  8 18:53:02 2014

@author: chu-chu
"""

import networkx as nx
import matplotlib.pyplot as plt
G = nx.Graph()
for i in range(5):
    for j in range(5):
        G.add_node((i,j))
        if i>0:
            G.add_edge((i-1,j),(i,j))
        if j>0:
            G.add_edge((i-1,j),(i,j-1))
nx.draw(G)
plt.show() 