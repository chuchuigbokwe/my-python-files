# -*- coding: utf-8 -*-
"""
Created on Wed Jan 21 01:58:41 2015

@author: chu-chu
"""

import numpy as np
import cv2
# Load image and load it
img = cv2.imread('messi5.jpg')
# grab the dimensions of the image and calculate the centre of the image
(h, w) = img.shape[:2]
centre = (w/2, h/2)

#rotate the image by 180 degrees
M = cv2.getRotationMatrix2D(centre, 180, 1.0)
rotated = cv2.warpAffine(img,M,(w,h))
cv2.imshow('rotated',rotated)
cv2.waitKey(0)