# -*- coding: utf-8 -*-
"""
Created on Sat Jan 17 13:58:13 2015

@author: chu-chu
"""

import numpy as np
import cv2

#img = cv2.imread('messi4.jpg',0) # 0 displays the image in grayscale
#cv2.imshow('image',img) #displays the image

grayImage = cv2.imread('chu-chu.jpg', cv2.CV_LOAD_IMAGE_GRAYSCALE)
cv2.imshow('image',grayImage) #displays the image
k = cv2.waitKey(0) & 0xFF
if k == 27:     #wait for ESC key to exit
    cv2.destroyAllWindows()
elif k == ord('s'): # wait for 's' key to save and exit
    cv2.imwrite('chu-chuGray.jpg',grayImage)
    cv2.destroyAllWindows()