# -*- coding: utf-8 -*-
"""
Created on Sat Jan 31 21:35:06 2015

@author: chu-chu
"""

import cv2
import numpy as np

filename = 'messi5.jpg'
img = cv2.imread(filename)
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)
dst = cv2.cornerHarris(gray,2,3,0.04)

#result is dilated for marking the corners, not important
dst = cv2.dilate(dst,None)

# Threshold for an optimal value, it may vary depending on the image.
img[dst>0.01*dst.max()]=[0,0,255]

cv2.imshow('dst',img)
if cv2.waitKey(0) & 0xff == 27:
    cv2.destroyAllWindows()
    
def ImageGradients(image1,image2):
    x = np.array([[-1,1],[-1,1]])
    y = np.array([[-1,-1],[1,1]])
    t = np.ones((2,2))
    gray_img1 =  cv2.cvtColor(image1,cv2.COLOR_BGR2GRAY)
    gray_img2 =  cv2.cvtColor(image2,cv2.COLOR_BGR2GRAY)
    Ix_m = 
    Iy_m = 
    It_m = 
