# -*- coding: utf-8 -*-
"""
Created on Wed Jan 21 01:40:44 2015

@author: chu-chu
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Jan 17 13:44:37 2015

@author: chu-chu
"""

import numpy as np
import cv2
# Load image and load it
img = cv2.imread('messi4.jpg')
#cv2.namedWindow('image',cv2.WINDOW_NORMAL)
#cv2.imshow('image',img)
# we need to keep in mind aspect ratio so the image does
# not look skewed or distorted -- therefore, we calculate
# the ratio of the new image to the old image
print img.shape
r = 100.0/ img.shape[1]
dim = (100, int(img.shape[0] * r))
# perform the actual resizing of the image and show it
resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
cv2.imshow('resized', resized)
cv2.waitKey(0)
cv2.destroyAllWindows()
