#!/usr/bin/env python
import rospy
import numpy as np
from std_msgs.msg import String
from geometry_msgs.msg import Twist

pub = rospy.Publisher('cmd_vel',Twist, queue_size=10)
rospy.init_node('master_splinter', anonymous=True)

def leonardo():
	T = 5.
	t = 0
	while True:
		rospy.sleep(0.01)
		t += 0.01
		V = 6. * np.pi * np.sqrt((np.cos((2.*np.pi * t)/T)**2 + 4.*np.cos((4.*np.pi * t)/T)**2) / T**2)
		w = 4. (* np.pi (3. * np.sin((2. * np.pi * t)/T) + np.sin((6.*np.pi*t)/T)))/ ( 5 + np.cos((4.* np.pi * t)/T) + 4 * np.cos((8.*np.pi * t)/T))
		p = Twist()
		p.linear.x = V
		p.angular.z = w   
		pub.publish(p)

def main():
	leonardo()

if __name__ == '__main__':
	main()