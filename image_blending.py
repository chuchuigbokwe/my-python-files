# -*- coding: utf-8 -*-
"""
Created on Tue Jan 27 00:43:13 2015

@author: chu-chu
"""
import numpy as np
import cv2

img1 = cv2.imread('messi5.jpg')
img2 = cv2.imread('messi4.jpg')
# resize both images to the same size so they can be merged

resized_img1 =  cv2.resize(img1,(400,400))
resized_img2 =  cv2.resize(img2,(400,400))

# dst = alpha.img1 + beta.img2 + gamma
# dst is equal to cv2.addWeighted
dst = cv2.addWeighted(resized_img1,0.7,resized_img2,0.3,0)

cv2.imshow('dst',dst)
cv2.waitKey(0)
cv2.destroyAllWindows()