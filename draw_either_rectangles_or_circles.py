# -*- coding: utf-8 -*-
"""
Created on Sat Jan 24 10:30:47 2015

@author: chu-chu
"""

import cv2
import numpy as np

drawing = False # true if mouse is pressed
mode = True # if True, draw rectangle. Press 'm' to toggle curve
ix,iy = -1,-1

# mouse callback function
def draw_circle(event, x,y,flags,param):
    '''
    draw either rectangles or circles (depending on the mode we select) by 
    dragging the mouse like we do in Paint application
    '''
    global ix,iy,drawing, mode
    
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True  #set drawing to true if the left mouse button is pressed
        ix,iy = x,y     # this sets both corners of the rectangle to the same point
                        # so the rectangle can be 'dragged out'
        
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing == True:     #to draw a rectangle you have to click and move the previous conditional changes drawing to true
            if mode == True:
                cv2.rectangle(img,(ix,iy),(x,y),(0,255,0),-1)
            else:
                cv2.circle(img,(x,y),5,(0,0,255),-1)
                
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        if mode == True:
            cv2.rectangle(img,(ix,iy),(x,y),(0,255,0),-1)
        else:
            cv2.circle(img,(x,y),5,(0,0,255),-1)
            
img = np.zeros([512,512,3])
cv2.namedWindow('image')
cv2.setMouseCallback('image',draw_circle)

while(1):
    cv2.imshow('image',img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('m'):
        mode = not mode # swithc modes, 'not mode' draws circles
    elif k == 27:
        break

cv2.destroyAllWindows()        
    