import sys      #sys is a library that you import
def collatz():
    '''
    Print the 3n+1 sequence from n, terminating when it reaches 1       
    '''
    longestSequence= 0  # we initialize these two values at zero
    longestInteger = 0
    for i in range(1,7):  
        n = i
        currentSequence = 0
        while n!= 1:            #this sequence divides n by 2 if its even and multiplies it by 3 and adds 1 provided n isn't 1
            if n % 2 == 0:
                n = n // 2
            else:
                n = n * 3 + 1
            currentSequence += 1 # q += 1 is the same as q = q +1. basically we 'bump' this variable by 1

        if  currentSequence > longestSequence:
            longestSequence = currentSequence
            longestInteger = i
        if i % 1000 == 0:
            sys.stdout.write('\b'*8)
            sys.stdout.write(str(i))
            sys.stdout.flush()

    print    
    print 'The longest integer is:', longestInteger
    print 'The longest Sequence is:',longestSequence
    
collatz()
     

    



