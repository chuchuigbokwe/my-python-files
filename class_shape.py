#!/usr/bin/env python
"""
Created on Sat Apr 25 09:14:26 2015

@author: chu-chu
"""

class Shape:
    def __init__(self,x,y):
        self.x = x
        self.y = y
        self.description = 'This shape has not been described yet'
        self.author = 'Nobody has claimed tomake this shape yet'
        
    def area(self):
        return self.x * self.y
    
    def perimeter(self):
        return 2 * (self.x + self.y)
        
    def describe(self,text):
        self.description = text
    
    def authorName(self,text):
        self.author = text
        
    def scaleSize(self,scale):
        self.x = self.x * scale
        self.y = self.y * scale