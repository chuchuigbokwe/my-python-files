# -*- coding: utf-8 -*-
"""
Created on Tue Oct 14 18:08:32 2014

@author: chu-chu
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Oct 10 11:28:15 2014

@author: chu-chu
"""

import numpy as np 
import matplotlib.pyplot as plt
x = 0
y = 0
theta = 0
vel = []
omega = []
timechange = []
xpr = []
ypr = []
thetapr = []
velerror = []
omegaerror = []
gammaerror = []
x_coordinates = [0]
y_coordinates = [0]
heading_angle = [0]
#alpha1 to 4 are robot specific motion error parameters determining velocity 
#noise
#alpha5 and 6 are robot specific motion error parameters determining the 
#standard deviation of additional rotational noise
alpha1 = np.random.uniform(0,1)
alpha2 = np.random.uniform(0,1)
alpha3 = np.random.uniform(0,1)
alpha4 = np.random.uniform(0,1)
alpha5 = np.random.uniform(0,1)
alpha6 = np.random.uniform(0,1)
redrobin = np.loadtxt('ds1_Odometry.dat')
batwing = np.loadtxt('ds1_Groundtruth.dat')
batman = np.loadtxt('ds1_Measurement.dat')
def simulatedController(v,w,delta_t):
    global x,y,theta,x_coordinates,y_coordinates
    theta = theta + w * delta_t 
    x = x + v * delta_t * np.cos(theta) 
    y = y + v * delta_t * np.sin(theta) 
    heading_angle.append(theta)
    x_coordinates.append(x)
    y_coordinates.append(y)
    #return x_coordinates,y_coordinates, heading_angle


def SimConVerification():
    ''' I verified my controllere with the values given in question 2'''
    nightwing = np.array([[0.5,0,1],[0,-1/np.pi*2,1],[0.5,0,1],[0,1/np.pi*2,1]\
    ,[0.5,0,1]])
    dim = np.shape(nightwing)
    row = 0
    column = 0
    for i in range(dim[0]): #parse through the matrix form left to right and
    #descend a row
        if row< dim[0] and column < dim[1]:
            v = nightwing[row][column]
            column +=1
            w = nightwing[row][column]
            column +=1
            delta_t = nightwing[row][column]
            simulatedController(v,w,delta_t)
            row +=1
            column = 0
        else:
            break
    print 'These are the x co-ordinates',x_coordinates
    print 
    print 'These are the y co-ordinates', y_coordinates
    print
    print 'These are the heading angles in radians', heading_angle
    
    plt.title('Verification of Simulated Controller with Values Given in \
    Question 2')
    plt.ylabel('Y-Axis')
    plt.xlabel('X-Axis')
    plt.plot(x_coordinates,y_coordinates)
    plt.show()
    
SimConVerification()


def SimConWithRobotDataset():
    
    dim = np.shape(redrobin)
    row = 0
    column = 0
    
    t1 = redrobin[0][0]
    for i in range(dim[0]):
        if row< dim[0] and column < dim[1]:
            if row == 0:
               delta_t = 0
               timechange.append(delta_t)
            else:
                delta_t = redrobin[row][column] - redrobin[row-1][column]
                timechange.append(delta_t)
            column +=1
            v = redrobin[row][column]
            column +=1
            w = redrobin[row][column]
            simulatedController(v,w,delta_t)
            vel.append(v)
            omega.append(w)
            row +=1
            column = 0
        else:
            break
    return timechange
SimConWithRobotDataset()

def Groundtruth():
    batwing = np.loadtxt('ds1_Groundtruth.dat')
    dim = np.shape(batwing)
    timestampground = [0]
    xaxis = [0]
    yaxis = [0]
    row = 0
    column = 0
    for i in range(dim[0]):
        if row< dim[0] and column < dim[1]:
            t = batwing[row][column]
            column +=1
            xvalue = batwing[row][column]
            column +=1
            yvalue = batwing[row][column]
            column +=1
            orientation = batwing[row][column]
            
            xaxis.append(xvalue)
            yaxis.append(yvalue)
            timestampground.append(t)
            row +=1
            column = 0
        else:
            break

    plt.title('ds1_Groundtruth.dat Plotted with Odometry from Simulated Controller')
    plt.ylabel('Y-Axis')
    plt.xlabel('X-Axis')
    plt.plot(xaxis,yaxis, x_coordinates,y_coordinates)
    plt.show()

Groundtruth()

def convtoarrays():
    xarray = np.asarray(x_coordinates)
    yarray = np.asarray(y_coordinates)
    thetaarray = np.asarray(heading_angle)
    a = np.vstack((xarray,yarray,thetaarray.T))
    print np.shape(a)     
    return a
    
    
convtoarrays()

#def MotionModel():
#    A = np.identity(3)
#    x =     
#    F = A*x + B*u
    

#def PredictionStep():
#    A = np..identity(3)
#    ohm = 0.1 * np.matlib.identity(3)
#    for i in range(len(x_coordintes)):
#        M = np.array([[x_coordintes[i],batwing.T[1][i]],[y_coordinates[i],batwing.T[2][i]],[heading_anle[i],batwing.T[3][i]]])
#        R = np.cov(M)          
#        ohmprime = np.dot(A,ohm,A.T) + R
#        psiprime = ohm * np.dot(A,ohmprime,psi) + [y_coordinates[i],np.dot(B,U)
#    print ohmprime
#
#PredictionStep()

