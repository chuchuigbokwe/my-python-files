# -*- coding: utf-8 -*-
"""
Created on Fri Nov 28 15:52:51 2014

@author: chu-chu
"""

import pygame
 
# Define some colors
BLACK    = (   0,   0,   0)
WHITE    = ( 255, 255, 255)
GREEN    = (   0, 255,   0)
RED      = ( 255,   0,   0)
 
# This sets the width and height of each grid location
width  = 6
height = 6

# This sets the margin between each cell
margin = 1
# Create a 2 dimensional array. A two dimensional
# array is simply a list of lists.
grid = []
for row in range(100):
    # Add an empty array that will hold each cell
    # in this row
    grid.append([])
    for column in range(100):
        grid[row].append(0) # Append a cell
 
# Set row 1, cell 5 to one. (Remember rows and
# column numbers start at zero.)
grid[0][0] = 1
 
# Initialize pygame
pygame.init()
 
# Set the height and width of the screen
size = [800, 1000]
screen = pygame.display.set_mode(size)
 
# Set title of screen
pygame.display.set_caption("Array Backed Grid")
 
#Loop until the user clicks the close button.
done = False
 
# Used to manage how fast the screen updates
clock = pygame.time.Clock()
 
# -------- Main Program Loop -----------
while done == False:
    for event in pygame.event.get(): # User did something
        if event.type == pygame.QUIT: # If user clicked close
            done = True # Flag that we are done so we exit this loop
        elif event.type == pygame.MOUSEBUTTONDOWN:
            # User clicks the mouse. Get the position
            pos = pygame.mouse.get_pos()
            
            # Change the x/y screen coordinates to grid coordinates
            column = pos[0] // (width + margin)
            row = pos[1] // (height + margin)
            
            # Set that location to zero
            grid[row][column] = 1
            print("Click ", pos, "Grid coordinates: ", row, column)
 
    # Set the screen background
    screen.fill(BLACK)
 
    # Draw the grid
    for row in range(100):
        for column in range(100):
            color = WHITE
            if grid[row][column] == 1:
                color = GREEN
                
            pygame.draw.rect(screen,color,[(margin+width)*column+margin,
#                                           (margin+height)*row+margin,width,height])
            pygame.draw.circle(screen,color,(pos[0],pos[1]),100,0)
 
    # Limit to 60 frames per second
    clock.tick(60)
 
    # Go ahead and update the screen with what we've drawn.
    pygame.display.flip()
 
# Be IDLE friendly. If you forget this line, the program will 'hang'
# on exit.
pygame.quit()

#pygame.draw.circle(Surface, color, pos, radius, width=0)
