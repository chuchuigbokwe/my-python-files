#!/usr/bin/env python
"""
Created on Tue Mar  3 22:17:11 2015

@author: chu-chu
"""

img = cv2.imread('messi5.jpg',0)
rows,cols = img.shape

M = cv2.getRotationMatrix2D((cols/2,rows/2),90,1)
dst = cv2.warpAffine(img,M,(cols,rows))

cv2.imshow('rotated image',dst)
cv2.imshow('img',img)
cv2.waitKey(0)
cv2.destroyAllWindows()