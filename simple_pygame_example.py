# -*- coding: utf-8 -*-
"""
Created on Sat Nov 29 14:23:21 2014

@author: chu-chu
"""

import pygame
def main():
    '''Set up the game and run the main game loop'''
    pygame.init()       #prepare the pygame module for use
    surface_sz = 480    #Desired physical surface size,in pixels.
    
#    Create surface of (width, height), and its window
    main_surface = pygame.display.set_mode((surface_sz,surface_sz))
#    set u o some data to describe a small rectangle and its color
    small_rect = (300, 200, 150, 90)
    some_color = (255, 0, 0)
    
    while True:
        ev = pygame.event.poll()    #Look for any event
        if ev.type == pygame.QUIT:  #Window close button clicked?
            break
#        Update your game objects and data structures here...
            
#       We draw everything form scratch on each frame
#       So first fill everything wiht the background color
        main_surface.fill((0,200,255))
            
#       Overpaint a smaller rectangle on the surface
        main_surface.fill(some_color,small_rect)
#       Now the surface is ready, tell pygame to display it!
        pygame.display.flip()
    pygame.quit()   #Once we leave the loop, close the window

main() 