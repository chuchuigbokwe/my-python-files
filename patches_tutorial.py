# -*- coding: utf-8 -*-
"""
Created on Thu Oct 23 20:25:51 2014

@author: chu-chu
"""

import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches

verts = [
    (0., 0.), # left, bottom
    (0., 1.), # left, top
    (1., 1.), # right, top
    (1., 0.), # right, bottom
    (0., 0.), # ignored
    ]

codes = [Path.MOVETO,
         Path.LINETO,
         Path.LINETO,
         Path.LINETO,
         Path.CLOSEPOLY,
         ]

path = Path(verts, codes)

fig = plt.figure()
ax = fig.add_subplot(111)
patch = patches.PathPatch(path, facecolor='orange', lw=2)
ax.add_patch(patch)
ax.set_xlim(-2,2)
ax.set_ylim(-2,2)
plt.show()

def square(x,y):
	verts = [
			(x, y), 			# left, bottom
			(x, y+ 1), 		# left, top
			(x + 1, y + 1), 	# right, top
			(x + 1, y + 0), 	# right, bottom
			(0., 0.), 							# ignored
			]

	codes = [Path.MOVETO,
			Path.LINETO,
			Path.LINETO,
			Path.LINETO,
			Path.CLOSEPOLY,
			]
	return verts, codes