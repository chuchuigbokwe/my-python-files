# -*- coding: utf-8 -*-
"""
Created on Fri Oct 10 23:43:51 2014

@author: chu-chu
"""

import numpy as np 
import matplotlib.pyplot as plt

def Groundtruth():
    a = np.loadtxt('ds1_Groundtruth.dat')
    dim = np.shape(a)
    x=[]
    y=[]
    for i in range(dim[0]):
        x.append(a[i][1])
        y.append(a[i][2])
        i += 1
        
    for i in range(dim[0]):
        plt.title('ds1_Groundtruth')
        plt.ylabel('Y-Axis')
        plt.xlabel('X-Axis')
        plt.plot(x,y)
        plt.show()
    
Groundtruth()
