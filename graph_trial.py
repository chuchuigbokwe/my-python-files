# -*- coding: utf-8 -*-
"""
Created on Fri Nov 28 13:20:43 2014

@author: chu-chu
"""

class Graph:
   def __init__(self):
      self.edges = {}
   
   def neighbors(self, id):
      return self.edges[id]
      
example_graph = Graph()
example_graph.edges = {
   'A': ['B'],
   'B': ['A', 'C', 'D'],
   'C': ['A'],
   'D': ['E', 'A'],
   'E': ['B']
}

import collections

class Queue:
   def __init__(self):
      self.elements = collections.deque()
   
   def empty(self):
      return len(self.elements) == 0
   
   def put(self, x):
      self.elements.append(x)
   
   def get(self):
      return self.elements.popleft()
      
      
def breadth_first_search(graph, start):
   frontier = Queue()
   frontier.put(start)
   visited = {}
   visited[start] = True
   
   while not frontier.empty():
      current = frontier.get()
      print("Visiting %r" % current)
      for next in graph.neighbors(current):
         if next not in visited:
            frontier.put(next)
            visited[next] = True

breadth_first_search(example_graph, 'A')