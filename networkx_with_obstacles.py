# -*- coding: utf-8 -*-
"""
Question 1 and 2
Created on Mon Dec  8 18:17:39 2014

@author: chu-chu
"""

from matplotlib import pyplot as plt
import networkx as nx
 
 
GRID_SIZE = [100, 100] #Builds a 100 by 100 graph
G = nx.grid_graph(dim=GRID_SIZE)
 
 
def obstacle(lower_left, width, height):
    """
    Given the (x,y) coord of the lower left corner of a box, and its width and
    height, return the coords covered by this box
    """
    x, y = lower_left
    for i in range(x, x + width):
        for j in range(y, y + height):
            yield (i, j)
            

#def CircularObstacle(centre, radius):
#    """
#    Given the (x,y) coord of a circle and the radius , return the coords covered by this box
#    """
#    x,y = centre
#    for i in range(50):
#        for j in range(50):
#            dist = (((centre[0] - x) ** 2 + (centre[1] - y) ** 2))**0.5
#            if dist <= radius:
#                yield(i,j)
# I couldn't get circles to work
def EuclideanDistance(a, b):
    (x1, y1) = a
    (x2, y2) = b
    return ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** 0.5
 
 
def gridplot(graph):
    """
    Plots the grid graph as black dots
    """
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111)
    x, y = zip(*G.nodes())
    ax.plot(x, y, 'k,')
    ax.axis('equal')
 
 
def pathplot(graph, start, stop, **kwargs):
    """
    Plots the shortest path via A* and returns the list of x and y coords.
    """
    path = nx.astar_path(G, start, goal, EuclideanDistance)
    x, y = zip(*path)
    plt.plot(x, y, **kwargs)
#    print path
    return path
 
# Create some obstacles and remove those points from consideration by deleting
# them from the graph
obstacles = [
    ((80, 90),20, 5),    
    ((42, 50),10,30),
    ((32, 40),60,25),
    ((10,15),35,15),  
    ((20,90),33,17)  
]
for lower_left, width, height in obstacles:
    for coord in obstacle(lower_left, width, height): #removes nodes in the obstacle
        try:
            G.remove_node(coord)
        except:
            continue

gridplot(G)
plt.xlim([0,100])
plt.ylim([0,100])
start = (10, 10)
goal = (99, 99)
pathplot(G, start, goal, color='r', linewidth=2)
plt.plot(start,goal)

print'These nodes traversed from start to goal are', nx.astar_path(G,start,goal,EuclideanDistance)
print'The total number of nodes explored is: ', nx.astar_path_length(G,start,goal,EuclideanDistance)


plt.show()

#alternative astar algorithm(buggy)
#
#def aStar(nodes, targets, edges):
#	NodesNotVisitedYet = np.vstack([nodes,targets]) 
#	TotalCost = []  
#	TrueCost= []  
#	expandedList = [] 
#	parent = [] 
#	VisitedNodes = [] 
#
#	start = targets[0]
#	goal = targets[1]
#	current = start
#	VisitedNodes.append(current)
#	parent.append(current)
#	for newEdge in edges[hash(str(current))]:
#		heuristic = evalHeuristic(newEdge, goal) 
#		trueCost = evalTrueCost(current,newEdge,0) 
#		totalCost = heuristic + trueCost
#		trueCostList.append(trueCost)
#		totalCostList.append(totalCost)
#		expandedList.append(newEdge)
#		parent.append(current)
#		touchedNodes.append(newEdge)
#	lowestCost = totalCost.index(min(totalCostList))
#	current = expandedList[lowestCost]
#	heuristic = evalHeuristic(current, goal)
#	parentCost = trueCostList[lowestCostIndex]
#	count = 0
#	while heuristic != 0:
#		expandedList.pop(lowestCost)
#		trueCost.pop(lowestCost)
#		totalCost.pop(lowestCost)
#		for newEdge in edges[hash(str(current))]:
#
#			skip = False
#			for touched in touchedNodes:	
#				try:
#					if newEdge[0] == touched[0] and newEdge[1] == touched[1]:
#						skip = True
#						break
#				except:
#					print "Game over."
#					skip = True
#					break
#			if skip:
#				continue
#			heuristic = evalHeuristic(newEdge, goal)
#			trueCost = evalTrueCost(current,newEdge,parentCost)
#			totalCost = heuristic + trueCost
#			trueCostList.append(trueCost)
#			totalCostList.append(totalCost)
#			expandedList.append(newEdge)
#			parentList.append(current)
#			touchedNodes.append(newEdge)
#		try:
#			lowestCostIndex = totalCostList.index(min(totalCostList))
#		except:
#			print 'Hitherto shalt thou come, but no further.'
#			return False
#			break
#		current = expandedList[lowestCostIndex]
#
#		heuristic = evalHeuristic(current, goal)
#		parentCost = trueCostList[lowestCostIndex]
#		count += 1
#		if count >50:
#			break
#	path = []
#	index = lowestCostIndex
#	count = 0
#	while round(current[0],6) != round(start[0],6) and round(current[1],6) != round(start[1],6):
#		for i in range(len(touchedNodes)):
#			if current[0] == touchedNodes[i][0] and current[1] == touchedNodes[i][1]:
#				index = i
#				break
#		parent = parentList[index]
#		path.append(parent)
#		current = parent
#		count += 1
#		if count > 20:
#			break
#	print path
#	current = goal
#	for i in path:
#		plt.plot([current[0],i[0]],[current[1],i[1]], linewidth = 2, color = 'g')
#		current = i
#
#radius = 0
