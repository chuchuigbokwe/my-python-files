# -*- coding: utf-8 -*-
"""
Created on Tue Oct 21 17:39:46 2014

@author: chu-chu
"""

import numpy as np
import matplotlib.pyplot as plt

def loadLandmarks():

    landmarks = []
    subject, x, y, x_std, y_std = np.loadtxt('ds1_Landmark_Groundtruth.dat', unpack = True)
    for i in range(0,len(subject)): 
        landmarks.append((round(subject[i]),round(x[i]),round(y[i])))
        #print '\nLandmarks: subject = ', subject[i], '(X = ', int(x[i]),', Y = ',int(y[i]),')'

    return landmarks


def landmarkplot():
    landmarks = np.loadtxt('ds1_Landmark_Groundtruth.dat')
    x = []
    y = []
    dim = np.shape(landmarks)
    for i in range(dim[0]):
        roundx = round(landmarks[i][1])
        roundy = round(landmarks[i][2])
        x.append(roundx)
        y.append(roundy)
        print x[i],y[i]
    fig = plt.figure()
    ax =fig.gca()
    ax.set_xticks(np.arange(-2,6,1))
    ax.set_yticks(np.arange(-6,7,1))     
    plt.title('Landmarks')
    plt.ylabel('Y-Axis')
    plt.xlabel('X-Axis')
    plt.scatter(x,y)
    #plt.plot(x,y,'ro') #'-b',linestyle='', marker = 'o') 
    #plt.axis('equal')  
    plt.xlim([-2,5])
    plt.ylim([-6,6])
    plt.grid(True,'major',linewidth=1)
    plt.show()
    
#    print x
#    print
#    print y 
    return x,y

#gca().add_patch(Rectangle((1,1),1,1))
#x{obs} - x[max]/xmax-xmin
#
#xmax-xobs/cell
#squareArray = rArray[:3:,:3,]
#>>> squareArray
#array([[  0,   1,   2],
#       [  4,  50,   6],
#       [  8,   9, 100]])
#>>> squareArray[0,0] *= 10
#>>> squareArray[1,1] *= 10
#>>> squareArray[2,2] *= 10
#>>> squareArray
#array([[   0,    1,    2],
#       [   4,  500,    6],
#       [   8,    9, 1000]])
def gridarray():
    landmarks = np.loadtxt('ds1_Landmark_Groundtruth.dat')
    grid = np.ones((13,8))
    robotx = []
    roboty = []
    xmin = -2
    ymax = 6
    for i in range(len(landmarks)):
        newx = round(x[i]) - xmin
        newy = abs(round(y[i]) + ymax - 12)
        robotx.append(newx)
        roboty.append(newy)
        grid[roboty[i],robotx[i]] *=1000   
    print grid
    print robotx
    print
    print roboty
    return robotx,roboty
    return grid

def main():
    loadLandmarks()
    landmarkplot()
    gridarray()
main()
 
 
def transform(x,y):
    xmin = -2
    ymax = 6
    new_coordinates = []
    newx = round(x) - xmin
    newy = abs(round(y) + ymax - 12)
    new_coordinates.append(newy)
    new_coordinates.append(newx)
    print new_coordinates, 'its in th y,x format'
    return new_coordinates
      
all_nodes = []
weights = []
for x in range(8):
    for y in range(13):
        all_nodes.append([x, y])
        weights.append(1)
        


def neighbours(node):
    dirs = [[-1,-1],[-1,0],[-1,1],[0,-1],[0,1],[1,-1],[1,0],[1,1]]
    result = []
    for i in range(len(dirs)):
        neighbour = [node[0] + dirs[i][0], node[1] + dirs[i][1]]
        #if neighbor in all_nodes:
        if neighbor[0] < 13 and 0 <= neighbour[1] < 8:
            result.append(neighbour)
        #result.append(neighbor)
    print result
    return result   