#!/usr/bin/env python
"""
Created on Fri Mar 20 12:05:34 2015

@author: chu-chu
"""

import cv2
import numpy as np

img = cv2.imread('messi5.jpg')

ORANGE_MIN = np.array([5, 50, 50],np.uint8)
ORANGE_MAX = np.array([15, 255, 255],np.uint8)

hsv_img = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)

frame_threshed = cv2.inRange(hsv_img, ORANGE_MIN, ORANGE_MAX)
cv2.imwrite('output2.jpg', frame_threshed)
cv2.imshow('threshed', frame_threshed)


#cv2.waitKey(0)
#cv2.destroyAllWindows()
#
#
#k = cv2.waitKey(1) & 0xFF
#if k == 27:
#   break
#
#cv2.destroyAllWindows()


 # this should be called always, frame or not.
#if cv2.waitKey(1) & 0xFF == ord('q'):
#   break
cv2.destroyAllWindows()