===================================================================
Modules Needed:
cv2, math, PIL, Numpy

Put utils.py in the same directory as the group1.py
===================================================================

===================================================================
How to run the program:
1) Open IDLE
2) import os
3) os.chdir(�gdirectory that contains the python program�h)
4) execfile("group1.py")
5) Enter the parameters:
For example (directory should be full path):

Enter images directory: D:\NUS Lecture Notes & Documents\Year 4 Sem

1\CS4243\Project\stitcher-master\stitcher-master\univ

Enter output directory: D:\NUS Lecture Notes & Documents\Year 4 Sem

1\CS4243\Project\stitcher-master\stitcher-master\univ

Enter key frame (full path): D:\NUS Lecture Notes & Documents\Year 4

Sem 1\CS4243\Project\stitcher-master\stitcher-master\univ\Univ4.jpg

Enter image width (resize): 300

Enter image type: jpg
===================================================================

===================================================================
Brief explanation

- key frame is the base image, that is the first image to be put into the panorama. The first stitching will be done between the key frame and the next nearest image.
We didn't manage to handle all the errors of our program, so for different set of images not all images can work as a keyframe. As a rule of thumb, we usually pick the image that is the "middle" of the panorama. 
Here are the working cases:
UTown Panorama: A (4).JPG 
Cenlib Stairs: any images are fine, but some pleasant looking one includes B(5).JPG and B (6).JPG
University Panorama: Univ3.jpg and Univ4.jpg
Shanghai Panorama: shanghai-15.png

Some errors include:
1) Memory allocation error
2) RANSAC error count>=4 this happens when RANSAC cannot return 4
points minimum needed to construct homography matrix
3) The numpy array of typenum=2, ndims=3 can not be created
4) ValueError: cannot convert float NaN to integer
5) The total matrix size does not fit to "size_t" type

These errors occurs when you choose the �gwrong�h keyframe. However, these errors appear almost randomly for shanghai images, even with the same configuration (same image size, same keyframe used). The program can stitch different amount of shanghai images each run, and it can stitch up to all images. 

- as there is memory limitation on our program for really big image,
we include resizing option for the images. However, scaling the
image down to a really small size also won't work all the time, as
we suspect less and less features are tracked, and cause RANSAC error.
Take note that the resizing is permanent, so keep a copy of the original images.

For our test images, we find that our code works with these size configurations:
UTown Panorama: width=300 or width=500
Cenlib Stairs: width=500 (more flexible)
University Panorama: width=300 or width=500
Shanghai Panorama: width=500

- our program read files in directory, so specify the images type to be read: JPG, jpg, or png