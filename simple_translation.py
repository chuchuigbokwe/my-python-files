#!/usr/bin/env python
"""
Created on Tue Mar  3 21:46:51 2015

@author: chu-chu
"""

import cv2
import numpy as np

img = cv2.imread('messi5.jpg',0)
rows,cols = img.shape

M = np.float32([[1,0,100],[0,1,50]])
dst = cv2.warpAffine(img,M,(cols,rows)) 
#use cols,rows Third argument of the cv2.warpAffine() function is the size of
#the output image, which should be in the form of (width, height). Remember 
#width = number of columns, and height = number of rows.

cv2.imshow('warped',dst)
cv2.imshow('img',img)
cv2.waitKey(0)
cv2.destroyAllWindows()