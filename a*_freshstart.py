# -*- coding: utf-8 -*-
"""
Created on Thu Oct 23 20:02:33 2014

@author: chu-chu
"""
import math
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.path import Path
import matplotlib.patches as patches
landmarks = np.loadtxt('ds1_Landmark_Groundtruth.dat')



#    print robotx
#    print
#    print roboty
#    return robotx,roboty
#    return grid


x = []
y = []

def square(x,y):
	verts = [
			(x, y), 			# left, bottom
			(x, y+ 1), 		# left, top
			(x + 1, y + 1), 	# right, top
			(x + 1, y + 0), 	# right, bottom
			(0., 0.), 							# ignored
			]

	codes = [Path.MOVETO,
			Path.LINETO,
			Path.LINETO,
			Path.LINETO,
			Path.CLOSEPOLY,
			]
	return verts, codes
 
def landmarkplot():
    fig = plt.figure()
    sp = fig.add_subplot(111)
    
    dim = np.shape(landmarks)
    for i in range(dim[0]):
        roundx = round(landmarks[i][1])
        roundy = round(landmarks[i][2])
        x.append(roundx)
        y.append(roundy)
        
        verts, codes = square(round(x[i]),round(y[i]))    
        path = Path(verts, codes)
        patch = patches.PathPatch(path, facecolor='orange', lw=2)
        sp.add_patch(patch)
        #print x[i],y[i]
#
    ax =fig.gca()
    ax.set_xticks(np.arange(-2,6,1))
    ax.set_yticks(np.arange(-6,7,1))
    plt.xlim([-2,5])
    plt.ylim([-6,6])
    plt.grid(True,'major',linewidth=1)     
    plt.title('Landmarks')
    plt.ylabel('Y-Axis')
    plt.xlabel('X-Axis')
    plt.scatter(x,y)    
    plt.show()
    #plt.plot(x,y,'ro') #'-b',linestyle='', marker = 'o') 
    plt.show()
    return x,y

landmarkplot()

grid = np.ones((13,8))
robotx = []
roboty = []
xmin = -2
ymax = 6
for i in range(len(landmarks)):
    newx = round(x[i]) - xmin
    newy = abs(round(y[i]) + ymax - 12)
    robotx.append(newx)
    roboty.append(newy)
    grid[roboty[i],robotx[i]] *=1000   
print grid


#def gridarray():
#    landmarks = np.loadtxt('ds1_Landmark_Groundtruth.dat')
#    grid = np.ones((13,8))
#    robotx = []
#    roboty = []
#    xmin = -2
#    ymax = 6
#    for i in range(len(landmarks)):
#        newx = round(x[i]) - xmin
#        newy = abs(round(y[i]) + ymax - 12)
#        robotx.append(newx)
#        roboty.append(newy)
#        grid[roboty[i],robotx[i]] *=1000   
##    print grid
##    print robotx
##    print
##    print roboty
#    return robotx,roboty
#    return grid


 
 
def transform(x,y):
    xmin = -2
    ymax = 6
    new_coordinates = []
    newx = round(x) - xmin
    newy = abs(round(y) + ymax - 12)
    new_coordinates.append(newy)
    new_coordinates.append(newx)
    print new_coordinates, 'its in th y,x format'
    return new_coordinates
      
all_nodes = []
weights = []
for i in range(8):
    for j in range(13):
        all_nodes.append([i, j])
        weights.append(1)
        


def neighbours(node):
    dirs = [[-1,-1],[-1,0],[-1,1],[0,-1],[0,1],[1,-1],[1,0],[1,1]]
    result = []
    for i in range(len(dirs)):
        neighbour = [node[0] + dirs[i][0], node[1] + dirs[i][1]]
        #if neighbor in all_nodes:
        if neighbour[0] < 13 and 0 <= neighbour[1] < 8:
            result.append(neighbour)
        #result.append(neighbor)
    print result
    return result 

  

#def main():
#    loadLandmarks()
#    landmarkplot()
#    gridarray()
#main()  
#def weightedNeighbours():
#    for i in range(len(robotx)):
#        
