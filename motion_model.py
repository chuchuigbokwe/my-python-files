# -*- coding: utf-8 -*-
"""
Created on Sun Oct 12 01:02:09 2014

@author: chu-chu
"""
import numpy as np

alpha1 = random.uniform(0,1)
alpha2 = random.uniform(0,1)
alpha3 = random.uniform(0,1)
alpha4 = random.uniform(0,1)
alpha5 = random.uniform(0,1)
alpha6 = random.uniform(0,1)

def sample(b):
    x= 0
    for i in range(12):
        x += np.random.uniform(-1,1)
    return b/6. * x
    
def motion_model():
    a12  = alpha1 * v + alpha2 * w
    a34 = alpha3 * v + alpha4 * w   
    a56 = alpha5 * v + alpha6 * w
    vdot = v + sample(q1)
    wdot = w + sample(q1)
    gammadot = sample(q2)
    xprime = x - v/w * np.sin(theta) + v/w * np.sin(theta + w * delta_t)
    yprime = y + v/w * np.cos(theta) - v/w * np.cos(theta + w * delta_t)
    thetaprime = theta + wdot * delta_t + gammadot * dt