#!/usr/bin/env python
'''
Create a slide show of images in a folder with smooth transition between 
images using cv2.addWeighted function
'''
import numpy as np
import cv2

img1 = cv2.imread('messi5.jpg')
img2 = cv2.imread('opencv_logo.png')
# resize both images to the same size so they can be merged

resized_img1 =  cv2.resize(img1,(400,400))
resized_img2 =  cv2.resize(img2,(400,400))

# dst = alpha.img1 + beta.img2 + gamma
# dst is equal to cv2.addWeighted
alpha = 0
beta = 1

for i in range(20):
    dst = cv2.addWeighted(resized_img1,alpha,resized_img2,beta,0)
    cv2.imshow('dst',dst)
    cv2.waitKey(0)
    if i <10:
        alpha += 0.1
        beta -= 0.1
    else:
        alpha -= 0.1
        beta += 0.1

cv2.destroyAllWindows()