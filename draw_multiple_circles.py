# -*- coding: utf-8 -*-
"""
Created on Sun Nov 30 12:33:28 2014

@author: chu-chu
"""

import pygame

background_colour = (255,255,255)
(width, height) = (300, 200)
BLUE =  (0, 0, 255)

class Particle:
    def __init__(self,colour, (x, y), size):
        self.x = x
        self.y = y
        self.size = size
        self.colour = (0, 0, 255)
        self.thickness = 0

    def display(self):
        pygame.draw.circle(screen, self.colour, (self.x, self.y), self.size, self.thickness)

screen = pygame.display.set_mode((width, height))
pygame.display.set_caption('Tutorial 2')
screen.fill(background_colour)

#my_first_particle = Particle((150, 50), 35)
#my_first_particle.display()
#
#pygame.display.flip()

#running = True
#while running:
#    for event in pygame.event.get():
#        if event.type == pygame.QUIT:
#            running = False
            
def draw_multiple_circles():
    how_many = int(input('Enter how many circles you want to draw:'))
    x_centre = []
    y_centre = []
    radius = []
    for i in range(how_many):
        x = int(input('Enter the x-coordinate of the circles centre:'))
        y = int(input('Enter the y-coordinate of the circles centre:'))
        r = int(input('Enter radius of the circles centre:'))
        x_centre.append(x)
        y_centre.append(y)
        radius.append(r)
        a_circle = Particle(BLUE,[x_centre[i],y_centre[i]],radius[i])
        a_circle.display()
    pygame.display.flip()
    
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

draw_multiple_circles()
    