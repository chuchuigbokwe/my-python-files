#!/usr/bin/env python
"""
Created on Sun Jun 21 17:44:33 2015

@author: chu-chu
"""

class Point:
    """ Point class represents and manipulates x,y coords. """
    
    def __init__(self,x=0,y=0):
        """Create a new point at the origin """
        self.x = x
        self.y = y
    
    def distance_from_origin(self):
        """Compute my distance from the origin """
        return ((self.x ** 2) + (self.y ** 2)) ** 0.5
        
    def __str__(self):
        return "({0},{1})".format(self.x,self.y)

    def halfway(self,target):
        """ Return the halfway point between myself and the target """
        mx = (self.x + target.x)/2
        my = (self.y + target.y)/2
        return Point(mx,my)
        
    def reflect_x(self):
        """
        Add a method reflect_x to Point which returns a new Point, one which is
        the reflection of the point about the x-axis. For example, 
        Point(3, 5).reflect_x() is (3, -5)
        """
        return Point(self.x, self.y * -1)
        
    def slope_from_origin(self):
        """
        Add a method slope_from_origin which returns the slope of the line 
        joining the origin to the point
        """
        try:
            slope = float(self.y)/(self.x)
            return slope
        except ZeroDivisionError:
            print 'division by zero, the point is already at the origin'

    def get_line_to(self,pt2):
        """
        The equation of a straight line is "y = ax + b", (or perhaps 
        "y = mx + c"). The coefficients a and b completely describe the line. 
        Write a method in the Point class so that if a point instance is given 
        another point, it will compute the equation of the straight line 
        joining the two points. It must return the two coefficients as a 
        tuple of two values. For example,
        >>> print(Point(4, 11).get_line_to(Point(6, 15)))
        >>> (2, 3)
        """
       
        try:
            m = float(self.y - pt2.y)/(self.x - pt2.x)
#            return m
        except ZeroDivisionError:
            print 'division by zero, the point is already at the origin'
        
        c = -m* pt2.x + pt2.y
        return Point(m,c)