# -*- coding: utf-8 -*-
"""
Created on Fri Nov 28 13:29:54 2014

@author: chu-chu
"""

class SquareGrid:
   def __init__(self, width, height):
      self.width = width
      self.height = height
      self.walls = []
   
   def in_bounds(self, id):
      (x, y) = id
      return 0 <= x < self.width and 0 <= y < self.height
   
   def passable(self, id):
      return id not in self.walls
   
   def neighbors(self, id):
      (x, y) = id
      results = [(x+1, y), (x, y-1), (x-1, y), (x, y+1)]
      if (x + y) % 2 == 0: results.reverse() # aesthetics
      results = filter(self.in_bounds, results)
      results = filter(self.passable, results)
      return results
      
g = SquareGrid(30, 15)
#DIAGRAM1_WALLS = [from_id_width(id, width=30) for id in [21,22,51,52,81,82,93,94,111,112,123,124,133,134,141,142,153,154,163,164,171,172,173,174,175,183,184,193,194,201,202,203,204,205,213,214,223,224,243,244,253,254,273,274,283,284,303,304,313,314,333,334,343,344,373,374,403,404,433,434]]
#g.walls = DIAGRAM1_WALLS # long list, [(21, 0), (21, 2), ...]
draw_grid(g)