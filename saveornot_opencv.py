# -*- coding: utf-8 -*-
"""
Created on Mon Jan 12 00:25:34 2015

@author: chu-chu
"""
import numpy as np
import cv2

def SaveOrNot():
    '''
    A program loads an image in grayscale, displays it, save the image 
    if you press ‘s’ and exit, or simply exit without saving if you press 
    ESC key.
    '''
    img = cv2.imread('messi5.jpg',0)
    cv2.imshow('image',img)
    k = cv2.waitKey(0) & 0xFF
    if k == 27:     #Wait for ESC key to exit
        cv2.destroyAllWindows(0)
    elif k == ord('s'): #Wait for 's' key to save and exit
        cv2.imwrite('messigray.png',img)
        cv2.destroyAllWindows()
        
SaveOrNot()