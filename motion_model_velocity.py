# -*- coding: utf-8 -*-
"""
Created on Sat Oct 11 15:21:30 2014

@author: chu-chu
"""
def prob(a,b):
    probability = 1/(np.sqrt(2 * np.pi * b)) * np.exp((-0.5 * a**2/b))

#def motion_model_velocity():
#    for i in range(11524):
#        totalprobability = []
#        absv = np.abs(vel[i])
#        absw = np.abs(omega[i])
#        a12 = alpha1 * absv + alpha2 * absw
#        a34 = alpha3 * absv + alpha4 * absw   
#        a56 = alpha5 * absv + alpha6 * absw
#        
#        xt_1 = x_coordinates[i-1]
#        yt_1 = y_coordinates[i-1]
#        thetat_1 = heading_angle[i-1]
#        
#        xprime = x_coordinates[i]
#        yprime = y_coordinates[i]
#        thetaprime = heading_angle[i]
#        if i ==0:
#            mu = 0
#        else:
#            mu = 0.5 * (((xt_1 - xprime)*np.cos(theta) + (yt_1 - yprime) * np.sin(theta))/ ((yt_1 - yprime)* np.cos(theta) - (xt_1 - xprime)* np.sin(theta)))
#        
#        xstar = (xt_1 + xprime)/2. + mu * (yt_1 - yprime)
#        ystar = (yt_1 + yprime)/2. + mu * (xprime - xt_1)
#        rstar = np.sqrt((xt_1 - xprime)**2 - (yt_1 - yprime)**2 )
#        
#        delta_theta = np.arctan2(yprime - yt_1, xprime - xt_1) - np.arctan2(y - ystar, x - xstar)
#        vstar = delta_theta / delta_t * rstar
#        
#        wstar = delta_theta / timechange[i]
#        gammastar = (thetaprime - thetat_1) / delta_t * wstar
#        totalprob = prob(vel[i]-vstar, a12) * prob(omega[i]-wstar,a34) * prob(gammastar,a56)
#        totalprobability.append[totalprob]
#        i += 1
#
#motion_model_velocity()



def prob(a,b):
    if a or b == 0 :
        probability = 1
    else:
        probability = 1/(np.sqrt(2 * np.pi * b**2)) * np.exp((-0.5 * a**2/b**2))
    return probability

def motion_model():
    ''' The motion model is a set of equations that show the behaviour of 
    the robot in real life conditions, i.e our initial controller with noise
    factored in he function prob(a,b) models the motion error.whic I'll add to
    my controller to give new values of my posterior position.This algorithmn
    was gotten from equations 5.26 - 5.28 and table 5.1 in Probabilistic Robotics by Sebastian 
    Thrun'''
    
    
    for i in range(1000):
        #absv or absw != 0:
        a12 = alpha1 * vel[i]**2 + alpha2 * omega[i]**2
        a34 = alpha3 * vel[i]**2 + alpha4 * omega[i]**2   
        a56 = alpha5 * vel[i]**2 + alpha6 * omega[i]**2
    
        xt_1 = x_coordinates[i-1]
        yt_1 = y_coordinates[i-1]
        thetat_1 = heading_angle[i-1]
        
        xprime = x_coordinates[i]
        yprime = y_coordinates[i]
        thetaprime = heading_angle[i]
        
        if xt_1 - xprime or yt_1 - yprime != 0: 
            mu = 0.5 * (((xt_1 - xprime)*np.cos(theta) + (yt_1 - yprime) * np.sin(theta))/ ((yt_1 - yprime)* np.cos(theta) - (xt_1 - xprime)* np.sin(theta)))
        
            xstar = float((xt_1 + xprime)/2. + mu * (yt_1 - yprime))
            ystar = float((yt_1 + yprime)/2. + mu * (xprime - xt_1))
            rstar = np.sqrt(((xt_1 - xstar)**2) - ((yt_1 - ystar)**2))
            
            delta_theta = np.arctan2(yprime - ystar, xprime - xstar) - np.arctan2(yt_1 - ystar, xt_1 - xstar)
            vstar = delta_theta / timechange[i] * rstar
            wstar = delta_theta / timechange[i]
            gammastar = (thetaprime - thetat_1) / timechange[i] * wstar
            
            velerr = vel[i]- vstar
            omegaerr = omega[i] - wstar
            gammaerr = gammastar
        else:
            velerr = 0
            omegaerr = 0
            gammaerr = 0
            
        velerror.append(velerr)
        omegaerror.append(omegaerr)
        gammaerror.append(gammaerr)
        i += 1
    print xxx
motion_model()

measurementmodelx = []
measurementmodely = []
measurementmodeltheta = []
#a12 = alpha1 * vel[i] + alpha2 * omega[i]
#a34 = alpha3 * vel[i] + alpha4 * omega[i]  
#a56 = alpha5 * vel[i] + alpha6 * omega[i]




def MotionModel():
    odometry = np.loadtxt('ds1_Odometry.dat')
    odometryT = odometry.T
    x = 0
    y = 0
    theta = 0
    for i in range(len(odometry)):
        vprime = odometryT[1][i] 
        wprime = odometryT[2][i] 
        gammaprime = 0
        E = np.random.uniform(0.01,0.05)
        vpr = vprime + E
        wpr = wprime + E
        gammapr = 
        if wprime == 0:
            thetaprime = heading_angle[i] + wprime * timechange[i] 
            xprime = x + vprime * timechange[i] * np.cos(heading_angle[i]) 
            yprime = y + vprime * timechange[i] * np.sin(heading_angle[i]) 
        else:
            xprime = x - (vprime/wprime)*np.sin(heading_angle[i]) + (vprime/wprime)*np.sin(heading_angle[i] + wprime * timechange[i])
            yprime = y + (vprime/wprime)*np.cos(heading_angle[i]) + (vprime/wprime)*np.cos(heading_angle[i] + wprime * timechange[i])
            thetaprime = heading_angle[i]+ wprime * timechange[i] 
        x += xprime
        y += yprime
        theta += thetaprime
        i +=1
    
        measurementmodelx.append(xprime)
        measurementmodely.append(yprime)
        measurementmodeltheta.append(thetaprime)
    
    plt.title('Motion Model')
    plt.ylabel('Y-Axis')
    plt.xlabel('X-Axis')
    plt.plot(measurementmodelx,measurementmodely)
    plt.show()

MotionModel()
    
    
    

       